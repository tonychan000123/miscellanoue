### References
1. [Danielmiessler](https://danielmiessler.com/study/tcpdump/#basic-communication)
2. [Linuxize](https://linuxize.com/post/tcpdump-command-in-linux/)


### Example 1 - list all possible network interface

```{bash}
tcpdump -D
```

Result
```
1.eth0 [Up, Running]
2.lo [Up, Running, Loopback]
3.any (Pseudo-device that captures on all interfaces) [Up, Running]
4.bluetooth-monitor (Bluetooth Linux Monitor) [none]
5.nflog (Linux netfilter log (NFLOG) interface) [none]
6.nfqueue (Linux netfilter queue (NFQUEUE) interface) [none]
7.dummy0 [none]
8.tunl0 [none]
9.sit0 [none]
10.bond0 [none]
```

### Example 2 - list packets sent and received of particular interface

```{bash}
tcpdump -i eth0 -v # v for verbose
```

Result
```
    DESKTOP-FHHVN21.mshome.net.domain > 172.25.65.138.45595: 37642 NXDomain* 0/0/0 (44)
00:04:30.187733 IP (tos 0x0, ttl 128, id 47490, offset 0, flags [none], proto UDP (17), length 127)
    DESKTOP-FHHVN21.mshome.net.domain > 172.25.65.138.54579: 9386 NXDomain 0/1/0 (99)
00:04:30.187783 IP (tos 0xc0, ttl 64, id 34801, offset 0, flags [none], proto ICMP (1), length 155)
    172.25.65.138 > DESKTOP-FHHVN21.mshome.net: ICMP 172.25.65.138 udp port 54579 unreachable, length 135
        IP (tos 0x0, ttl 128, id 47490, offset 0, flags [none], proto UDP (17), length 127)
    DESKTOP-FHHVN21.mshome.net.domain > 172.25.65.138.54579: 9386 NXDomain 0/1/0 (99)
00:04:31.713935 IP (tos 0x0, ttl 128, id 47491, offset 0, flags [none], proto UDP (17), length 182)
...
```

### Example 3 - Find traffic by IP

This is going to monitor all traffic related to google.com, regardless of request to google.com or response from google.com 

Noted that host keyword is required to monitor traffic of source and destination
```{bash}
tcpdump -i eth0 -v host google.com
```

### Example 4 - Find traffic by source and destination

Only monitor source IP
```{bash}
tcpdump -i eth0 -v src 172.25.65.138
```

Only monitor destination
```{bash}
tcpdump -i eth0 -v dst 172.25.65.138
```

### Example 5 - Find traffic by subnet
Only monitor packets within particular range of subnet.

This is going to monitor all those packets from 172.25.65.0 to 172.25.65.255
```{bash}
tcpdump -i eth0 -v net 172.25.65.0/24
```

### Example 6 - Find traffic by protocol and subnet

```{bash}
tcpdump -i eth0 -v tcp and net 172.25.65.0/24
```

### Example 7 - Filter by IP and port 
```{bash}
tcpdump -i eth0 -v tcp src port 443 and dst 172.25.65.138
```

### Example 8 - Wirte packet result to both stream output and file

[Noted that `-` is the stdout stream](https://stackoverflow.com/questions/3797795/does-mean-stdout-in-bash)
```{bash}
tcpdump -w - -U | tee ./traffic-2.pcap | tcpdump -r -
```