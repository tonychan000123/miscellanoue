### Example 1 - use of BEGIN and END block in awk

[`marks.txt`](./resources/marks.txt)
```
1) Amit Physics 80
2) Rahul Maths 90
3) Shyam Biology 87
4) Kedar English 85
5) Hari History 89
```

Command 
```bash
awk 'BEGIN{printf "SrNo\tName\tSub\tMarks\n"} {print}' marks.txt
```

Result
- added titles to the output
```
Sr No   Name    Sub     Marks
1) Amit Physics 80
2) Rahul Maths 90
3) Shyam Biology 87
4) Kedar English 85
5) Hari History 89
```

### Example 2 - run awk script in a file

[`program_file.txt`](./resources/program_file.txt)

```bash
BEGIN{printf "SrNo\tName\tSub\tMarks\n"} {print}
```

Command 
```bash
awk -f ./program_file.txt marks.txt
```

### Example 3 - use with END block in awk
[`program_file_1.txt`](./resources/program_file_1.txt)
```bash
BEGIN {
    printf"==========begin=========\n"
} 

#Rules 
{
    print
} 

END {
    printf"==========end========="
}
```

Result
```
==========begin=========
1) Amit Physics 80
2) Rahul Maths 90
3) Shyam Biology 87
4) Kedar English 85
5) Hari History 89
==========end=========
```

Example 4 - pattern match 

```bash
awk '/a/ { print $0 }' marks.txt
```

Result
- lines that don't have 'a' in the files are filtered out (case sensitive)
```
2) Rahul Maths 90
3) Shyam Biology 87
4) Kedar English 85
5) Hari History 89
```